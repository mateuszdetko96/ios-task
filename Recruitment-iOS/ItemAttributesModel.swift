//
//  ItemAttributesModel.swift
//  Recruitment-iOS
//
//  Created by Mateusz Detko on 16/01/2022.
//  Copyright © 2022 Untitled Kingdom. All rights reserved.
//

struct ItemAttributesModel: Codable {
    let name: String
    let preview: String
    let color: ColorModel?
}
