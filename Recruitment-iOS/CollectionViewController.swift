//
//  CollectionViewController.swift
//  Recruitment-iOS
//
//  Created by Mateusz Detko on 16/01/2022.
//  Copyright © 2022 Untitled Kingdom. All rights reserved.
//

import UIKit

final class CollectionViewController: UICollectionViewController {

    private let flowLayout = CollectionViewFlowLayout()
    private var itemModels: [ItemModel] = []

    init() {
        super.init(collectionViewLayout: flowLayout)

        tabBarItem.title = "Collection"
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        flowLayout.delegate = self
        collectionView.register(CollectionViewCell.self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchItems()
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemModels.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CollectionViewCell = collectionView.dequeueCell(for: indexPath)
        let itemModel = itemModels[indexPath.row]
        cell.setup(item: itemModel)
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let itemModel = itemModels[indexPath.row]
        let viewController = DetailsViewController(itemModel: itemModel)
        navigationController?.pushViewController(viewController, animated: true)
    }

    private func fetchItems() {
        NetworkingManager.sharedManager.downloadItems { [weak self] items in
            self?.itemModels = items
            self?.collectionView.reloadData()
        }
    }
}

extension CollectionViewController: CollectionViewFlowLayoutDelegate {

    func previewTextForItem(at indexPath: IndexPath) -> String {
        return itemModels[indexPath.row].attributes.preview
    }
}
