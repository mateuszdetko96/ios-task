//
//  DetailsViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

final class DetailsViewController: BaseViewController {

    private let itemModel: ItemModel
    private let textView = UITextView()

    init(itemModel: ItemModel) {
        self.itemModel = itemModel
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        fetchData()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        textView.text = "Loading..."
    }

    override func addSubviews() {
        view.addSubview(textView)
    }

    override func setupSubviews() {
        view.backgroundColor = itemModel.attributes.color?.color ?? .black

        textView.backgroundColor = .clear
        textView.isEditable = false
        textView.font = .systemFont(ofSize: 14)

        setupTitle()
    }

    override func setupConstraints() {
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        textView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        textView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        textView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }

    private func setupTitle() {
        title = itemModel.attributes.name
            .enumerated()
            .map { index, letter in
                return index % 2 == 0 ? letter.uppercased() : letter.lowercased()
            }
            .joined()
    }

    private func fetchData() {
        NetworkingManager.sharedManager.downloadItemWithIdentifier(itemModel.identifier) { [weak self] details in
            self?.textView.text = details?.attributes.desc
        }
    }
}
