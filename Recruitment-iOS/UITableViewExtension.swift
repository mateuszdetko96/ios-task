//
//  UITableViewExtension.swift
//  Recruitment-iOS
//
//  Created by Mateusz Detko on 16/01/2022.
//  Copyright © 2022 Untitled Kingdom. All rights reserved.
//

import UIKit

extension UITableView {

    func dequeueCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Error: Cell with id: \(T.reuseIdentifier) for indexPath: \(indexPath) is not \(T.self)")
        }
        return cell
    }

    func register<T: UITableViewCell>(_ cellClass: T.Type) {
        register(cellClass, forCellReuseIdentifier: T.reuseIdentifier)
    }
}
