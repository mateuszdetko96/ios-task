//
//  ItemModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

struct ItemModel: Codable {
    let identifier: String
    let attributes: ItemAttributesModel

    private enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case attributes
    }
}
