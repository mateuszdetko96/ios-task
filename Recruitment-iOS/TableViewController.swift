//
//  TableViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

final class TableViewController: UITableViewController {
    
    private var itemModels: [ItemModel] = []

    init() {
        super.init(style: .plain)

        tabBarItem.title = "List"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UITableViewCell.self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        fetchItems()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueCell(for: indexPath)
        let itemModel = itemModels[indexPath.row]
        cell.backgroundColor = itemModel.attributes.color?.color ?? .black
        cell.textLabel?.text = itemModel.attributes.name
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let itemModel = itemModels[indexPath.row]
        let viewController = DetailsViewController(itemModel: itemModel)
        navigationController?.pushViewController(viewController, animated: true)
    }

    private func fetchItems() {
        NetworkingManager.sharedManager.downloadItems { [weak self] items in
            self?.itemModels = items
            self?.tableView.reloadData()
        }
    }
}
