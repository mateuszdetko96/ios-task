//
//  IntExtension.swift
//  Recruitment-iOS
//
//  Created by Mateusz Detko on 16/01/2022.
//  Copyright © 2022 Untitled Kingdom. All rights reserved.
//

extension Int {
    
    func times(block: (Int) -> Void) {
        for index in 0 ..< self {
            block(index)
        }
    }
}
