//
//  CollectionViewFlowLayout.swift
//  Recruitment-iOS
//
//  Created by Mateusz Detko on 16/01/2022.
//  Copyright © 2022 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol CollectionViewFlowLayoutDelegate: AnyObject {

    func previewTextForItem(at indexPath: IndexPath) -> String
}

final class CollectionViewFlowLayout: UICollectionViewFlowLayout {

    private enum Constants {
        static let numberOfColumns: Int = 2
        static let itemSpacing: CGFloat = 8
    }

    weak var delegate: CollectionViewFlowLayoutDelegate?

    private var cache: [UICollectionViewLayoutAttributes] = []

    private var contentWidth: CGFloat {
        guard let width = collectionView?.bounds.width else { return .zero }
        return width - sectionInset.left - sectionInset.right
    }

    private var contentHeight: CGFloat = .zero

    override init() {
        super.init()

        sectionInset = UIEdgeInsets(top: 0, left: Constants.itemSpacing, bottom: 0, right: Constants.itemSpacing)
        minimumLineSpacing = Constants.itemSpacing
        minimumInteritemSpacing = Constants.itemSpacing
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var collectionViewContentSize: CGSize {
      return CGSize(width: contentWidth, height: contentHeight)
    }

    override func prepare() {
        guard let collectionView = collectionView else { return }
        cache.removeAll()

        let columnWidth = (contentWidth - CGFloat(Constants.numberOfColumns - 1) * minimumInteritemSpacing) / CGFloat(Constants.numberOfColumns)

        var xOffsets: [CGFloat] = []
        var yOffsets: [CGFloat] = .init(repeating: .zero, count: Constants.numberOfColumns)

        Constants.numberOfColumns.times { column in
            if column == .zero {
                xOffsets.append(sectionInset.left)
            } else {
                xOffsets.append((xOffsets.last ?? .zero) + columnWidth + minimumInteritemSpacing)
            }
        }

        collectionView.numberOfSections.times { section in
            collectionView.numberOfItems(inSection: section).times { item in
                let indexPath = IndexPath(item: item, section: section)
                let column = yOffsets.firstIndex(of: yOffsets.min() ?? .zero) ?? .zero
                let itemHeight = calculateCellHeight(width: columnWidth, indexPath: indexPath)
                let frame = CGRect(x: xOffsets[column], y: yOffsets[column], width: columnWidth, height: itemHeight)

                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = frame
                cache.append(attributes)

                contentHeight = max(contentHeight, frame.maxY)
                yOffsets[column] += itemHeight + minimumLineSpacing
            }
        }
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return cache.filter { $0.frame.intersects(rect) }
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache.first { $0.indexPath == indexPath }
    }

    private func calculateCellHeight(width: CGFloat, indexPath: IndexPath) -> CGFloat {
        let text: String = delegate?.previewTextForItem(at: indexPath) ?? ""
        let textView = UITextView()
        textView.font = .systemFont(ofSize: 12)
        textView.text = text
        let textSize: CGSize = textView.sizeThatFits(CGSize(width: width, height: .greatestFiniteMagnitude))

        return textSize.height + 22
    }
}
