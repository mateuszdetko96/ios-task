//
//  ItemDetailsAttributes.swift
//  Recruitment-iOS
//
//  Created by Mateusz Detko on 16/01/2022.
//  Copyright © 2022 Untitled Kingdom. All rights reserved.
//

struct ItemDetailsAttributes: Codable {
    let name: String
    let color: ColorModel?
    let desc: String
}
