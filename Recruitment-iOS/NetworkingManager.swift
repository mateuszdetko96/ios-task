//
//  NetworkingManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

final class NetworkingManager: NSObject {

    static let sharedManager = NetworkingManager()

    private override init() {}
    
    func downloadItems(completion: @escaping (([ItemModel]) -> Void)) {
        request(filename: "Items.json") { (result: Result<ItemsDataModel, Error>) in
            switch result {
            case let .success(model):
                completion(model.data)
            case .failure:
                completion([])
            }
        }
    }
    
    func downloadItemWithIdentifier(_ identifier: String, completion: @escaping ((ItemDetailsModel?) -> Void)) {
        let filename = "Item\(identifier).json"
        request(filename: filename) { (result: Result<ItemDetailsDataModel, Error>) in
            switch result {
            case let .success(model):
                completion(model.data)
            case .failure:
                completion(nil)
            }
        }
    }

    private func request<T: Codable>(filename: String, completion: @escaping ((Result<T, Error>) -> Void)) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            completion(JSONParser.jsonFromFilename(filename))
        }
    }
}
