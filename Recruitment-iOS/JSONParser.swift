//
//  JSONParser.swift
//  Route1
//
//  Created by Paweł Sporysz on 11.12.2015.
//  Copyright © 2015 Untitled Kingdom. All rights reserved.
//

import Foundation

final class JSONParser {

    static func jsonFromFilename<T: Decodable>(_ filename: String) -> Result<T, Error> {
        guard let filepath = Bundle.main.path(forResource: filename, ofType: "") else {
            return .failure(NSError())
        }

        do {
            let stringContent = try String(contentsOfFile: filepath, encoding: String.Encoding.utf8)

            guard let data = stringContent.data(using: String.Encoding.utf8) else {
                return .failure(NSError())
            }

            let json = try JSONDecoder().decode(T.self, from: data)
            return .success(json)
        } catch _ {
            return .failure(NSError())
        }
    }
}
