//
//  StartupViewController.swift
//  Recruitment-iOS
//
//  Created by Mateusz Detko on 16/01/2022.
//  Copyright © 2022 Untitled Kingdom. All rights reserved.
//

import UIKit

final class StartupViewController: BaseViewController {

    private let startButton = UIButton(type: .system)

    override func addSubviews() {
        view.addSubview(startButton)
    }

    override func setupSubviews() {
        view.backgroundColor = .white

        startButton.setTitle("START", for: .normal)
        startButton.addTarget(self, action: #selector(didTapStart), for: .touchUpInside)
    }

    override func setupConstraints() {
        startButton.translatesAutoresizingMaskIntoConstraints = false
        startButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        startButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }

    @objc private func didTapStart() {
        navigationController?.pushViewController(TabBarController(), animated: true)
    }
}
