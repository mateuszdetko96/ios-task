//
//  CollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by Mateusz Detko on 16/01/2022.
//  Copyright © 2022 Untitled Kingdom. All rights reserved.
//

import UIKit

final class CollectionViewCell: UICollectionViewCell {

    private let titleLabel = UILabel()
    private let previewTextView = UITextView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        setupSubviews()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup(item: ItemModel) {
        backgroundColor = item.attributes.color?.color ?? .black
        titleLabel.text = item.attributes.name
        previewTextView.text = item.attributes.preview
    }

    private func addSubviews() {
        [titleLabel, previewTextView].forEach(contentView.addSubview)
    }

    private func setupSubviews() {
        titleLabel.textAlignment = .center
        
        previewTextView.backgroundColor = .clear
        previewTextView.isEditable = false
        previewTextView.isSelectable = false
        previewTextView.isUserInteractionEnabled = false
        previewTextView.font = .systemFont(ofSize: 12)
    }

    private func setupConstraints() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true

        previewTextView.translatesAutoresizingMaskIntoConstraints = false
        previewTextView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        previewTextView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        previewTextView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        previewTextView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
}
