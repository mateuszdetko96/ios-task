//
//  UICollectionReusableViewExtension.swift
//  Recruitment-iOS
//
//  Created by Mateusz Detko on 16/01/2022.
//  Copyright © 2022 Untitled Kingdom. All rights reserved.
//

import UIKit

extension UICollectionReusableView {

    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
