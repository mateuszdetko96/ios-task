//
//  ExtensionsTests.swift
//  Recruitment-iOSTests
//
//  Created by Mateusz Detko on 16/01/2022.
//  Copyright © 2022 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class ExtensionsTests: XCTestCase {

    final class TestCollectionViewCell: UICollectionViewCell {}
    final class TestTableViewCell: UITableViewCell {}

    func testReuseIdentifiers() {
        XCTAssertEqual(TestCollectionViewCell.reuseIdentifier, "TestCollectionViewCell")
        XCTAssertEqual(TestTableViewCell.reuseIdentifier, "TestTableViewCell")
    }

    func testRegisterAndDequeueCells() {
        let tableView = UITableView()
        tableView.register(TestTableViewCell.self)
        _ = tableView.dequeueCell(for: IndexPath(row: 0, section: 0)) as TestTableViewCell

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewLayout())
        collectionView.register(TestCollectionViewCell.self)
        _ = collectionView.dequeueCell(for: IndexPath(row: 0, section: 0)) as TestCollectionViewCell
    }
}
