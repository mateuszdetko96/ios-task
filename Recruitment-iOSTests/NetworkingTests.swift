//
//  NetworkingTests.swift
//  Recruitment-iOSTests
//
//  Created by Mateusz Detko on 16/01/2022.
//  Copyright © 2022 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class NetworkingTests: XCTestCase {

    func testDownloadItems() {
        let expectation = XCTestExpectation()

        NetworkingManager.sharedManager.downloadItems { items in
            XCTAssert(!items.isEmpty)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 3)
    }

    func testDownloadItemWithIdentifier() {
        let expectation = XCTestExpectation()
        expectation.expectedFulfillmentCount = 2

        NetworkingManager.sharedManager.downloadItemWithIdentifier("1") { item in
            XCTAssertNotNil(item)
            expectation.fulfill()
        }

        NetworkingManager.sharedManager.downloadItemWithIdentifier("0") { item in
            XCTAssertNil(item)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 3)
    }

    func testJSONParser() {
        let result1: Result<ItemsDataModel, Error> = JSONParser.jsonFromFilename("Items.json")
        if case .failure = result1 {
            XCTFail("Items.json file not exists")
        }

        let result2: Result<ItemDetailsDataModel, Error> = JSONParser.jsonFromFilename("Item1.json")
        if case .failure = result2 {
            XCTFail("Item1.json file not exists")
        }
    }
}
